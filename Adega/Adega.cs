﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adega
{
    public class Adega
    {
        private int prateleiras;
        private int nichos;
        private int quantidade;
        private int maximo;

        Garrafa[,] estante;


        public Adega(int _prateleiras, int _nichos)
        {
            prateleiras = _prateleiras;
            nichos = _nichos;
            quantidade = 0;
            maximo = _prateleiras * _nichos;
            estante = new Garrafa[prateleiras, nichos];
        }


        public bool Inserir(Garrafa garrafa)
        {
            if (quantidade == maximo)
                return false;

            int i, j;

            for (i = 0; i < prateleiras; i += 1)
                for (j = 0; j < nichos; j += 1)
                    if (estante[i, j] == null)
                    {
                        estante[i, j] = garrafa;
                        return true;
                    }

            return false;
        }


        public bool Inserir(Garrafa garrafa, int prateleira, int nicho)
        {
            if (estante[prateleira, nicho] == null)
            {
                estante[prateleira, nicho] = garrafa;
                return true;
            }

            return false;
        }


        // TODO: fazer
        // Obter a quantidade de vinhos (por tipo) que estão na adega;
        public int QuantidadeGarrafa(int tipo)
        {
            int quantidade = 0, i, j;
            for (i = 0; i < prateleiras; i += 1)
                for (j = 0; j < nichos; j += 1)
                    if (estante[i, j] == null)
                    {
                        quantidade += 1;
                    }

            return quantidade;
        }


        public int QuantidadeGarrafa()
        {
            return quantidade;
        }


        // TODO: fazer
        // Obter o vinho mais antigo da adega(menor data de fabricação);
        public Garrafa PegarGarrafaMaisVelha()
        {
            Garrafa garrafa = estante[0, 0];
            estante[0, 0] = null;
            return garrafa;
        }


        public Garrafa PegarGarrafa(int prateleira, int nicho)
        {
            Garrafa garrafa = estante[prateleira, nicho];
            estante[prateleira, nicho] = null;
            return garrafa;
        }


        // TODO: fazer
        // Obter o valor agregado da adega (somatório dos vinhos);
        public float ValorTotal()
        {
            float valor = 0.00F;

            return valor;
        }
    }
}
