﻿using System;

namespace Adega
{
    public class Garrafa
    {
        private int tipo;
        private string marca;
        private int volume;
        private DateTime ano;
        private float valor;

        public Garrafa(int _tipo, string _marca, int _volume, DateTime _ano, float _valor)
        {
            tipo = _tipo;
            marca = _marca;
            volume = _volume;
            ano = _ano;
            valor = _valor;
        }
    }
}
